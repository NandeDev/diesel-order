import { Meteor } from 'meteor/meteor';
import ReactDOM from 'react-dom';
import React from 'react';
import { Tracker } from 'meteor/tracker';
import { Session } from 'meteor/session';
import { AppRouter, history} from '../imports/Routes/AppRouter';
import '../imports/startup/simple-schema-config.js';


//make use of the own tracker auto run for every selected POD and Diesel Order. Everytime an item is selected the
// trackerOutrun fires and replaces the URL with the Selected ID
Tracker.autorun(() => {
    const selectedPodId = Session.get('selectedPodId');
    Session.set('isNavOpen', false);

     if (selectedPodId) {
        history.replace(`/proof-of-delivery/${selectedPodId}`);
    }
});


//make use of the own tracker auto run for every selected POD and Diesel Order. Everytime an item is selected the
// trackerAutorun fires and replaces the URL with the Selected ID
Tracker.autorun(() => {
    const selectedTownId = Session.get('selectedTownId');
    Session.set('isNavOpen', false);

    if (selectedTownId) {
        history.replace(`/Towns/${selectedTownId}`);
    }
});


//Tracker Autorun to check when the navbar gets clicked and changes the state of the navbar
Tracker.autorun(() => {
    const isNavOpen = Session.get('isNavOpen');
    document.body.classList.toggle('is-nav-open', isNavOpen);
});


//make use of the own tracker auto run for every selected Address. Everytime an item is selected the
// trackerAutorun fires and replaces the URL with the Selected ID
Tracker.autorun(() => {
    const selectedAddressId = Session.get('selectedAddressId');
    Session.set('isNavOpen', false);
    if (selectedAddressId) {
        history.replace(`/addresses/${selectedAddressId}`);
    }

});

//Set the states of the SelectedID variable
Meteor.startup(() => {
    Session.set('selectedNoteId', undefined);
    Session.set('selectedPodId', undefined);
    Session.set('selectedTownId', undefined);
    Session.set('selectedAddressId', undefined);
    Session.set('showVisible',true);
    Session.set('isNavOpen', false);
    ReactDOM.render(<AppRouter />, document.getElementById('app'));
});
