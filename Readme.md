# Diesels App

This is a diesels application built using meteor

## Getting started 

This app requires you to have meteor installed on your machine, then you can clone the repo and run the following  
```
meteor npm install
```

```
meteor 
```

## Running the tests 

Running the tests  is easy, all you have to do is run the following command 
and view the localhost:3000.
```
npm test 
```