import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import Signup from '../ui/Signup';
import Dashboard from '../ui/ScreenDieselOrder';
import NotFound from '../ui/NotFound';
import Login from '../ui/Login';
import POD from '../ui/ScreenPOD';
import Maps from '../ui/ScreenRoute';
import TownFile from "../ui/ScreenTown";
import Link from '../ui/ScreenAddress';
export const history = createHistory()


export const AppRouter = () => (

    <Router history={history}>
        <Switch>
            <PublicRoute path="/" component={Login} exact={true} />
            <PublicRoute path="/signup" component={Signup} />
            <PrivateRoute path="/proof-of-delivery" component={POD} />
            <PrivateRoute path="/dashboard" component={Dashboard} exact={true} />
            <PrivateRoute path="/dashboard/:id" component={Dashboard}/>
            <PrivateRoute path="/maps" component={Maps}/>
            <PrivateRoute path="/Towns" component={TownFile}/>
            <PrivateRoute path="/addresses" component={Link}/>
            <Route path="*" component={NotFound} />
        </Switch>
    </Router>
);
