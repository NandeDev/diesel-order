import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {Meteor} from "meteor/meteor";
import {Pods} from './../api/pods';
import PropTypes from 'prop-types';
import PodListHeader from "./PODListHeader";
import PodListItem from "./PODListItem";
import PODListEmpty from './PODListEmpty';


        export const PODList = (props)=>{
            return(
                <div className="item-list">
                    <PodListHeader/>
                    { props.pods.length === 0 ? <PODListEmpty/>: undefined}
                    {props.pods.map((pod)=>{
                        return(<PodListItem key={pod._id} pod={pod}/>)
                    })}

                </div>
            );
        };



        PODList.propTypes = {
            pods: PropTypes.array
        }

        export default createContainer(()=>{
            const selectedPodId = Session.get('selectedPodId');
            Meteor.subscribe('pods');
            let searchOption = {};
            return{
                pods: Pods.find(searchOption, {sort:{timestamp:-1}}).fetch().map((pod)=>{
                    // ...pod is a spread operator that allows us to keep everything we have in pod
                    return{
                        ...pod,
                        selected: pod._id === selectedPodId
                    }
                })

            };
        }, PODList);