import React from 'react';
import PropTypes from 'prop-types';
import {Meteor} from "meteor/meteor";
import Modal from "react-modal";
import {createContainer} from "meteor/react-meteor-data";
import {Session} from "meteor/session";
import createHistory from "history/createBrowserHistory";
import {Addresses} from '../api/addresses';


        export class AddressListItem extends React.Component{


            constructor(props){
                //create a state for the sidenav to see if its open or not
                //create an error state to manage errors
                super(props);
                this.state = {
                    isOpen:false,
                    error: ''
                };
            }

            onSubmit(e){
                const Address = this.state.address;
                const Code = this.state.code;
                const Description = this.state.description;

                e.preventDefault();
                if(Address !=='') {
                    Meteor.call('addresses.update', Address, Code, Description, (error, res) => {
                        if (!error) {
                            this.setState({
                                isOpen: false,
                                address: '',
                                description: '',
                                code: '',
                                error: ''
                            });//IF no error then clear the input field and close the modal
                        } else {
                            this.setState({error: error.reason});
                        }
                    });
                }else{

                    alert("Address may not be blank");
                }
            }


            Change(e){
                this.setState({
                    address: document.getElementById('pac-input').value,
                    code:document.getElementById('code').value,
                    description:document.getElementById('description').value

                });
            }

            onRequest(){
                this.setState({
                    isOpen:false,
                    address:'',
                    description:'',
                    code:'',
                    error:''
                });
            }

            render(){
                const className = this.props.selected ? 'item item--selected' : 'item';

                return(
                    <div>
                    <div className={className} onClick={()=>{
                        this.props.Session.set('selectedAddressId', this.props._id);
                    }}>
                        <h5 className="item__title">{this.props.Code}</h5>
                        <p className="item__subtitle">{this.props.Description}</p>
                        <br></br>
                        <p className="item__subtitle">{this.props.Address}</p>
                       </div>
                        <Modal
                            isOpen={this.state.isOpen}
                            contentLabel='addAddresses'
                            onRequestClose={this.onRequest.bind(this)}
                            className='boxed-view__box__edit'
                            overlayClassName='boxed-view boxed-view--modal'>
                            <h2>Edit Address</h2>
                            {this.state.error ?<p>{this.state.error}</p>:undefined}
                            <form onSubmit={this.onSubmit.bind(this)} className='boxed-view__form'>
                                <input ref='code' id="code"  className="controls" type="text" placeholder="Code"  value={this.props.Code}/>
                                <input ref='description' id="description"  className="controls" type="text" placeholder="Description" onChange={this.Change.bind(this)} value={this.props.Description}/>
                                <input ref='address' id="pac-input" className="controls" type="text"
                                       placeholder="Search Province"
                                       onChange={this.Change.bind(this)} placeholder='Type in an Address' value={this.props.Address} />
                                <div className="hidden">
                                    <div id="map"></div>
                                </div>
                                <button className='button'>Edit Address</button>
                                <button type='button' className='button button--secondary' onClick={()=>{this.setState({isOpen:false})}}>Exit</button>
                            </form>
                        </Modal>
                    </div>
                );
            }
        };

        AddressListItem.proptype = {

            _id: PropTypes.string.isRequired,
            Address: PropTypes.string.isRequired,
            userId: PropTypes.string.isRequired,
            selectedAddressId: PropTypes.string,
            Session: PropTypes.object.isRequired,
            Code: PropTypes.string.isRequired,
            Description: PropTypes.string.isRequired

        };

export default createContainer(()=>{

    const selectedAddressId = Session.get('selectedAddressId');
    return{

        selectedAddressId,
        address: Addresses.findOne(selectedAddressId),
        call:Meteor.call,
        history: createHistory(),
        Session: Session

    }

},AddressListItem);