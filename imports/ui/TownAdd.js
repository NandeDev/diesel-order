import React from 'react';
import {Meteor} from "meteor/meteor";


            export default class TownAdd extends React.Component{

                constructor(props){
                    super(props);
                    //create a state for the sidenav to see if its open or not
                    //create an error state to manage errors
                    this.state = {
                        isOpen:false,
                        error: ''
                    }
                }

                onSubmit(e){
                    //On submit we store the values of the inputs into variables and call the towns.insert method
                    // to insert the variables containing data
                    const Address = document.getElementById('autocomplete').value;
                    const Code =  document.getElementById('code').value;
                    const Description = document.getElementById('description').value;
                    const City = document.getElementById('locality').value;
                    const Country = document.getElementById('country').value;
                    const Province = document.getElementById('administrative_area_level_1').value;
                    const Latitude = document.getElementById('latitude').value;
                    const Longitude = document.getElementById('longitude').value;
                    e.preventDefault();
                        Meteor.call('towns.insert', Address, Code, Description, City, Country,Province,Latitude,Longitude,(error, res) => {
                            if (!error) {
                                if(res){
                                    this.props.Session.set({selectedTownId:res});
                                }
                                this.setState({
                                    isOpen: false,
                                    address: '',
                                    description: '',
                                    code: '',
                                    latitude:'',
                                    longitude:'',
                                    error: ''
                                });//IF no error then clear the input field and close the modal
                            } else {
                                this.setState({error: error.reason});
                            }
                        });
                }


                // componentDidMount is invoked immediately after a component is mounted (inserted into the tree).
                // Initialization that requires DOM nodes should go here. If you need to load data from a remote endpoint,
                // this is a good place to instantiate the network request.
                // This method is a good place to set up any subscriptions. If you do that, don’t forget to unsubscribe in componentDidMount
                //This method executes the method that calls the Maps API
                componentDidMount() {
                    ApiCallBackAddressAutoFill();
                }

                render(){
                    return (
                        <div className=''>
                            <button onClick={()=>{ var x = document.getElementById("addBlock");
                                document.getElementById('MapBox').style.display = 'none';
                                if (x.style.display === "block") {
                                    x.style.display = "none";
                                } else {
                                    x.style.display = "block";
                                }
                                document.getElementById('code').value = '';
                                document.getElementById('description').value = '';
                                document.getElementById('street_number').value = '';
                                document.getElementById('locality').value = '';
                                document.getElementById('administrative_area_level_1').value = '';
                                document.getElementById('postal_code').value = '';
                                document.getElementById('country').value = '';
                                document.getElementById('latitude').value = '';
                                document.getElementById('longitude').value = '';
                                document.getElementById('autocomplete').value = '';
                            }} className='button button--clear'>New Town</button>
                            <div id='addBlock' className='editor__add boxed-view__box__address'>
                                <h2>Add Town</h2>
                                {this.state.error ?<p>{this.state.error}</p>:undefined}
                                <form onSubmit={this.onSubmit.bind(this)} className='boxed-view__form'>

                                    <button className='button' onClick={()=>{if(document.getElementById('locality').value !== '' && document.getElementById('code').value !== '' &&document.getElementById('description').value !== ''){document.getElementById('addBlock').style.display = 'none';}}}>Add Town</button>

                                    <input type='text' id="code" className="controls" placeholder='Town Code'
                                          />
                                    <input ref='description' id="description"  className="controls" type="text" placeholder="Description" />
                                    <input id="autocomplete"
                                            type='text' className="controls" placeholder='Search Town'
                                    />

                                    <input type='text'className="controls" id="street_number" placeholder='Street Address'
                                           disabled={true}hidden={true}/>
                                    <input className="controls" id="route" disabled={true} hidden={true}/>

                                    <input type='text'className="controls" id="locality" disabled={true}  placeholder='City'
                                          />

                                    <input type='text'className="controls " id="administrative_area_level_1" placeholder='Province'
                                           disabled={true} />

                                    <input className="controls " id="postal_code" disabled={true} hidden={true} />

                                    <input type='text'className="controls " id="country" placeholder='Country'
                                           disabled={true}/>

                                    <input type='text'className="controls " id="latitude" placeholder='latitude'
                                           disabled={true}/>

                                    <input type='text'className="controls " id="longitude" placeholder='longitude'
                                           disabled={true}/>

                                    <div id="MapBox2" className="">
                                        <input id="pac-input" className="controls editor__input" type="text"
                                               placeholder="Search Province"
                                        />
                                        <div id="map"></div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    );
                }
            }
