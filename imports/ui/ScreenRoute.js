import React from 'react';
import PrivateHeader from './NavBar';
import MapsEditor from './MapsEditor';




export default () =>{


    return (
        <div>
            <PrivateHeader title='Maps'/>
            <MapsEditor/>
            </div>

    );
};