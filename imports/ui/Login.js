import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor'
import {createContainer} from 'meteor/react-meteor-data'
import PropTypes from 'prop-types';



        export class Login extends React.Component{

            constructor(props){
                super(props);
                this.state = {
                    error:''
                };
            }
            OnSubmit(e){
                e.preventDefault(); //prevents full page refresh
                let email = this.refs.refemail.value.trim();
                let password = this.refs.refpassword.value.trim();



                this.props.loginWithPassword({email}, password, (err) =>{

                        if(err.reason === 'Match failed'){
                            this.setState({error: 'Unable to login. Check email and password'});
                        }
                        else  if(err.reason === 'User not found'){
                            this.setState({error: 'Unable to login. Check email and password'});
                        }else{
                            this.setState({error: ''})
                        }


                });
            }
            render(){

                return (

                    <div className="boxed-view">
                        <div className='boxed-view__box'>
                        <h1>Login Screen</h1>
                        {this.state.error ? <p>{this.state.error}</p> : undefined} {/*if theres no error than render the paragraph tag */}
                        <form className='boxed-view__form' onSubmit={this.OnSubmit.bind(this)}>
                            {/*references allow us to target elements inside and outside the form */}
                            <input type='email'ref='refemail' name='email'  placeholder=' Enter Email' />
                            <input type='password' ref='refpassword' name='password'  placeholder=' Enter Password' />
                            <button className='button'>Login</button>
                        </form>
                        <Link to="/signup">Need an account?</Link>
                        </div>
                    </div>
                );

            }
        }

        Login.propTypes = {
            loginWithPassword: PropTypes.func
        };

            export default createContainer(()=>{
                return{
                    loginWithPassword: Meteor.loginWithPassword //allows us to use loginWithPassword as a prop instead of calling Meteor
                };
            }, Login);