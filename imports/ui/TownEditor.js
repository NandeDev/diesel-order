import React from 'react';
import PropTypes from 'prop-types';
import {createContainer} from 'meteor/react-meteor-data';
import {Session} from 'meteor/session';
import {Towns} from '../api/towns';
import createHistory from 'history/createBrowserHistory';
import {Meteor} from "meteor/meteor";



    //This component is used to view addresses based on the input from the user
        export class TownEditor extends React.Component {
            constructor(props) {
                super(props);
                this.state = {
                    address:''
                };
            }

            //we use this component to replace the address that is used by the view location function
            componentDidUpdate(prevProps, prevState, snapshot) {
                const currentNoteId = this.props.town ? this.props.town._id : undefined;
                const prevNoteId = prevProps.town ? prevProps.town._id : undefined;

                //this if statement checks the current and previous selected
                // note and sets the state of the input field to whatever address is selected
                if(currentNoteId && currentNoteId !== prevNoteId ){
                    this.setState({
                        Address:this.props.town.Address
                    })
                }
            }
            // componentDidMount is invoked immediately after a component is mounted (inserted into the tree).
            // Initialization that requires DOM nodes should go here. If you need to load data from a remote endpoint,
            // this is a good place to instantiate the network request.
            // This method is a good place to set up any subscriptions. If you do that, don’t forget to unsubscribe in componentDidMount
            //This method executes the method that calls the Maps API

            componentDidMount() {
                    ApiCallBackAddressAutoFill();
            }


            render() {
                    return (<div className="editor">
                        <button className="button" onClick={() => {
                            var x = document.getElementById("MapBox");
                            document.getElementById('addBlock').style.display = 'none';
                            if (x.style.display === "block") {
                                x.style.display = "none";
                            } else {
                                x.style.display = "block";
                            }
                        }}>View Location
                        </button>

                        <div id="MapBox" className="">
                            <input id="pac-input3" className="controls editor__input" type="text"
                                   placeholder="Search Province"
                                   value={this.state.Address} />
                            <div id="map4"></div>
                        </div>
                        <div id="map2"></div>
                    </div>);
                }
        };

        TownEditor.propTypes = {
            town: PropTypes.object,
            selectedTownId: PropTypes.string,
            history: PropTypes.object,
            call:PropTypes.func
        };

        export default createContainer(()=>{
            const selectedTownId = Session.get('selectedTownId');
            return{

                selectedTownId,
                town: Towns.findOne(selectedTownId),
                call:Meteor.call,
                history: createHistory()
            }

        },TownEditor);