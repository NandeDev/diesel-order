import React from 'react';
import {Link} from 'react-router-dom';
import { Accounts } from 'meteor/accounts-base'
import {createContainer} from 'meteor/react-meteor-data'
import PropTypes from 'prop-types';


    export class Signup extends React.Component{

        constructor(props){
            super(props);
            this.state = {
                error:''
            };
        }

        OnSubmit(e){
            e.preventDefault(); //prevents full page refresh
            let email = this.refs.refemail.value.trim();
            let password = this.refs.refpassword.value.trim();
            //If statement to check the password length on button click
            if(password.length < 3){
                return this.setState({error:'Password must be more than 3 characters'});
            }

            this.props.createUser({email, password}, (err)=>{
                if(err){
                    this.setState({error: err.reason});
                }
                else {
                    this.setState({error:''});
                }
            });
        }


        render(){
            return (

                <div className="boxed-view">
                    <div className='boxed-view__box'>
                    <h1>Sign up</h1>
                    {this.state.error ? <p>{this.state.error}</p> : undefined} {/*if theres no error than render the paragraph tag */}
                    <form className='boxed-view__form' onSubmit={this.OnSubmit.bind(this)} noValidate>
                        {/*references allow us to target elements inside and outside the form */}
                        <input type='email' ref='refemail' name='email' placeholder=' Enter Email' />
                        <input type='password' ref='refpassword' name='password' placeholder=' Enter Password' />
                        <button className='button'>Create Account</button>
                    </form>
                    <Link to="/">Already have an account?</Link>
                    </div>
                </div>

            );

        }
    }
            Signup.propTypes = {
            createUser: PropTypes.func.isRequired
            };

            export default createContainer(()=>{
                return{
                    createUser: Accounts.createUser
                };
            }, Signup)