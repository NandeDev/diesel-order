import React from 'react';
import PropTypes from 'prop-types';
import {createContainer} from 'meteor/react-meteor-data';
import {Session} from 'meteor/session';
import createHistory from 'history/createBrowserHistory';
import {Meteor} from "meteor/meteor";
import {Addresses} from "../api/addresses";



        //This component is used to view addresses based on the input from the user
        export class AddressEditor extends React.Component {
            constructor(props) {
                super(props);
                this.state = {
                    address:''
                };
            }

            //we use this component to replace the address that is used by the view location function
            componentDidUpdate(prevProps, prevState, snapshot) {
                const currentNoteId = this.props.address ? this.props.address._id : undefined;
                const prevNoteId = prevProps.address ? prevProps.address._id : undefined;

                //this if statement checks the current and previous selected
                // note and sets the state of the input field to whatever address is selected
                if(currentNoteId && currentNoteId !== prevNoteId ){
                    this.setState({
                        Address:this.props.address.Address
                    })
                }
            }
            // componentDidMount is invoked immediately after a component is mounted (inserted into the tree).
            // Initialization that requires DOM nodes should go here. If you need to load data from a remote endpoint,
            // this is a good place to instantiate the network request.
            // This method is a good place to set up any subscriptions. If you do that, don’t forget to unsubscribe in componentDidMount
            //This method executes the method that calls the Maps API

            componentDidMount() {
                ApiCallBackAddressAutoFill();
            }


            render() {
                return (<div className="editor">
                    <button className="button" onClick={() => {
                        var x = document.getElementById("MapBoxAddress");
                        document.getElementById('AddressAddBlock').style.display = 'none';
                        if (x.style.display === "block") {
                            x.style.display = "none";
                            document.getElementById('AddressAddBlock').style.display = 'block';
                        } else {
                            x.style.display = "block";
                            document.getElementById('AddressAddBlock').style.display = 'none';
                        }
                    }}>View Location
                    </button>

                    <div id="MapBoxAddress" className="">
                        <input id="pac-input3" className="controls editor__input" type="text"
                               placeholder="Search Province"
                               value={this.state.Address}
                        />
                        <div id="map4"></div>
                    </div>
                    <div id="map2"></div>
                </div>);
            }
        };

        AddressEditor.propTypes = {
            address: PropTypes.object,
            selectedAddressId: PropTypes.string,
            history: PropTypes.object,
            call:PropTypes.func
        };

        export default createContainer(()=>{
            const selectedAddressId = Session.get('selectedAddressId');
            return{

                selectedAddressId,
                address: Addresses.findOne(selectedAddressId),
                call:Meteor.call,
                history: createHistory()
            }

        },AddressEditor);