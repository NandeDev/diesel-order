import React from 'react';
import {Meteor} from "meteor/meteor";


        export default class AddressAdd extends React.Component{

            constructor(props){
                super(props);
                //create a state for the sidenav to see if its open or not
                //create an error state to manage errors
                this.state = {
                    isOpen:false,
                    error: ''
                }
            }

            //onSubmit gets executed when the form gets submitted.
            onSubmit(e){
                const Address = document.getElementById('pac-input').value;
                const Code =  this.state.code;
                 const Description = this.state.description;
                e.preventDefault();
                // addresses.insert is called to insert the values from the input fields
                    Meteor.call('addresses.insert', Address, Code, Description, (error, res) => {
                        if (!error) {
                            if(res){
                                this.props.Session.set({selectedAddressId:res});
                            }
                            this.setState({
                                isOpen: false,
                                address: '',
                                description: '',
                                code: '',
                                error: ''
                            });
                            //If no error then clear the input field
                        } else {
                            this.setState({error: error.reason});
                        }
                    });

            }


            //The Change method checks the input fields and sets the attributes to the value on the input fields
            // The change method is used on the OnChange Handler on the input field
            Change(e){
               this.setState({
                   address: document.getElementById('pac-input').value,
                   code:document.getElementById('code').value,
                   description:document.getElementById('description').value
               });
            }

            // componentDidMount is invoked immediately after a component is mounted (inserted into the tree).
            // Initialization that requires DOM nodes should go here. If you need to load data from a remote endpoint,
            // this is a good place to instantiate the network request.
            // This method is a good place to set up any subscriptions. If you do that, don’t forget to unsubscribe in componentDidMount
            //This method executes the method that calls the Maps API
            componentDidMount() {
                ApiCallBackAddressSearch();
            }


            render(){
                return (
                    <div id="AddressAddBlock">
                        {/*<button onClick={()=>{Meteor.call('addresses.remove')}} className='button button--clear'>Clear List</button>*/}
                        <div className='boxed-view__box__address'>
                        <h2>Add an Address</h2>
                            {this.state.error ?<p>{this.state.error}</p>:undefined}
                        <form  onSubmit={this.onSubmit.bind(this)} className='boxed-view__form'>
                            <input type='text' id="code" className="controls" placeholder='Address Code' onChange={this.Change.bind(this)} value={this.state.code}/>
                            <input ref='description' id="description"  className="controls" type="text" placeholder="Description" onChange={this.Change.bind(this)} value={this.state.description}/>
                            <input ref='address' id="pac-input" className="controls" type="text" onChange={this.Change.bind(this)} value={this.state.address} placeholder='Type in an Address' />
                            <div className="hidden">
                            <div id="map"></div>
                            </div>
                            <button className='button'>Add Address</button>
                            <div id="MapBox" className="">
                                <input id="pac-input" className="controls editor__input" type="text" placeholder="Search Province"/>
                                <div id="map"></div>
                            </div>
                        </form>
                        </div>

                    </div>
                );
            }
        }