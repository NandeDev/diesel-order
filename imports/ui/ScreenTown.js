import React from 'react';
import PrivateHeader from './NavBar';
import TownList from "./TownList";
import TownAdd from "./TownAdd";
import TownEditor from "./TownEditor";






//lest create a stateless functional component for our Addresses class

export default () =>{
    return (
        <div>
            <PrivateHeader title='Towns'/>
            <div className="page-content">
                <div className="page-content__sidebar">
                    <TownList/>
                </div>
                <div className="page-content__main">
                    <TownAdd/>
                </div>
                <div className="">
                    <TownEditor/>
                </div>
            </div>
        </div>
    );
};


