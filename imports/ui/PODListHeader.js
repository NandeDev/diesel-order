import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {Meteor} from "meteor/meteor";
import PropTypes from 'prop-types';
import {Session} from 'meteor/session';



        export const PODListHeader = (props)=>{

            return(<div className="item-list__header">
                    <button className="button" onClick={() =>{ if(confirm('Are you sure?') ==true){
                        props.meteorCall('pods.insert', (err,res)=>{
                            if(res){props.Session.set({selectedPodId:res});
                            }})}}}>New POD</button>
                </div>
            );
        };


        PODListHeader.propTypes = {
            meteorCall: PropTypes.func.isRequired,
            Session: PropTypes.object.isRequired

        }

        export default createContainer(()=>{ // create container wraps the function in tracker Autorun
            return{ //return an object
                meteorCall:  Meteor.call,
                Session: Session
            };

        }, PODListHeader);