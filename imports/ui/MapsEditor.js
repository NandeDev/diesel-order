import React from 'react';
import {createContainer} from 'meteor/react-meteor-data';


        export class MapsEditor extends React.Component{

            // componentDidMount is invoked immediately after a component is mounted (inserted into the tree).
            // Initialization that requires DOM nodes should go here. If you need to load data from a remote endpoint,
            // this is a good place to instantiate the network request.
            // This method is a good place to set up any subscriptions. If you do that, don’t forget to unsubscribe in componentDidMount
            //This method executes the method that calls the Maps API

            componentDidMount() {
                    ApiCallBackRouteCalculation();
            }

            render() {
                return(
                    <div>
                        <div id="map3"></div>
                            <div id='right-way'>
                                <div id=''>
                                    <b>Waypoints:</b> <br></br>
                                    <select multiple id="waypoints">
                                        <option value="BRACKENFELL, sa">BRACKENFELL</option>
                                        <option value="BUTTERWORTH, sa">CAPE TOWN</option>
                                        <option value="EAST LONDON, sa">EAST LONDON</option>
                                        <option value="KIMBERLEY, sa">KIMBERLEY</option>
                                        <option value="MASERU">MASERU</option>
                                    </select>
                                </div>
                                <b>Start:</b>
                                <input id="start" value={Session.get('value')}></input>
                                <br></br>
                                <b>End:</b>
                                <input id="end" value={Session.get('value2')}></input>
                                <br></br>
                            <div id="button">
                                <input id="button" className='button button--link' type="submit" id="submit"/>
                            </div>
                        </div>
                            {/*The directions panel displays the route information after submission*/}
                            <div id="directions-panel"></div>
                    </div>
                );
            }

        }
export default createContainer(()=>{
    return{
    }
},MapsEditor);