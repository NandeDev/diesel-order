import React from 'react';
import { Addresses } from '../api/addresses';
import {Meteor} from 'meteor/meteor';
import AddressListItem from './AddressListItem';
import {Session} from 'meteor/session';
import {createContainer} from 'meteor/react-meteor-data';
import PropTypes from "prop-types";


        export const AddressList = (props)=>{
            if(props.addresses.length === 0){
                return(<div className='link-view-list'><p className='link--paragraph'>Empty list</p></div>)
            }else {
                return props.addresses.map((address) => {
                    return <div className="item-list"> <AddressListItem key={address._id} {...address}/></div>;
                });
            }
        };


        AddressList.propTypes = {
            addresses: PropTypes.array
        };


    export default createContainer(()=>{// create container wraps the function in tracker Autorun
        const selectedAddressId = Session.get('selectedAddressId');
        Meteor.subscribe('addresses');
        return{
            addresses: Addresses.find().fetch().map((address)=>{
                // ...address is a spread operator that allows us to keep everything we have in address
                return{
                    ...address,
                    selected: address._id === selectedAddressId
                }
            })
        };
    }, AddressList);