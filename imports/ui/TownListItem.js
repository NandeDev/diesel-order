import PropTypes from 'prop-types';
import React from 'react';
import { Session } from 'meteor/session';
import { createContainer } from 'meteor/react-meteor-data';



            export const TownListItem = (props) => {

                const className = props.town.selected ? 'item item--selected' : 'item';
                return (

                    <div className={className} onClick={() => {props.Session.set('selectedTownId', props.town._id);}}>
                        <h5 className="item__title">{ props.town.Code || 'Untitled Town' }</h5>
                        <h5  className="item__title">{props.town.Description}</h5>
                        <h5  className="item__title">{props.town.Province}</h5>
                        <h5  className="item__title">{props.town.Address}</h5>
                        <h5  className="item__title">{props.town.Latitude}</h5>
                        <h5  className="item__title">{props.town.Longitude}</h5>
                    </div>
                );
            };


            TownListItem.propTypes = {
                town: PropTypes.object.isRequired,
                Session: PropTypes.object.isRequired
            };

            export default createContainer(() => {
                return { Session };

            }, TownListItem);