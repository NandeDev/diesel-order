import React from 'react';
import { Link } from 'react-router-dom';


            //Simple message box that shows when entering a non-existant page

            export default ()=>{
                return (
                    <div className="boxed-view">
                    <div className='boxed-view__box'>
                    <h1>Page Not Found My Padawan</h1>
                        <p className='boxed-view__paragraph'>These are not the pages you are looking for</p>
                        <Link className='button button--link' to='/'>Head Home</Link>
                    </div>
                    </div>

                );
            };

