import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';
import { Session } from 'meteor/session';
import { createContainer } from 'meteor/react-meteor-data';

        export const PODListItem = (props) => {

            const className = props.pod.selected ? 'item item--selected' : 'item';

            return (
                <div className={className} onClick={() => {props.Session.set('selectedPodId', props.pod._id);}}>
                    <h5 className="item__title">{ props.pod.title || 'Untitled note' }</h5>
                    <p  className="item__subtitle">{ moment(props.pod.timestamp).format('M/DD/YY') }</p>
                </div>
            );
        };

        PODListItem.propTypes = {
            pod: PropTypes.object.isRequired,
            Session: PropTypes.object.isRequired
        };

        export default createContainer(() => {
            return { Session };
        }, PODListItem);