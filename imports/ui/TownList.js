import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {Meteor} from "meteor/meteor";
import {Towns} from './../api/towns';
import PropTypes from 'prop-types';
import TownListItem from "./TownListItem";
import PODListEmpty from './PODListEmpty';

            export const TownList = (props)=>{
                return(
                    <div className="item-list">
                        { props.towns.length === 0 ? <PODListEmpty/>: undefined}
                        {props.towns.map((town)=>{
                            return(<div className="item-list"><TownListItem key={town._id} town={town}/></div>)
                        })}
                    </div>
                );
            };

            TownList.propTypes = {
                towns: PropTypes.array
            }

            export default createContainer(()=>{
                const selectedTownId = Session.get('selectedTownId');
                Meteor.subscribe('towns');
                let searchOption = {};
                return{
                    towns: Towns.find(searchOption, {sort:{timestamp:-1}}).fetch().map((town)=>{
                        // ...town is a spread operator that allows us to keep everything we have in town
                        return{
                            ...town,
                            selected: town._id === selectedTownId
                        }
                    })
                };
            }, TownList);