import React from 'react';
import PrivateHeader from './NavBar';
import PODEditor from "./PODEditor";
import PODList from "./PODList";
import {TownEditor} from "./TownEditor";



export default () =>{

    return (
        <div>
            <PrivateHeader title='POD'/>
            <div className="page-content">
                <div className="page-content__sidebar">
                <PODList/>
                </div>
                <div className="page-content__main">
                    <PODEditor/>
                </div>
            </div>
        </div>
    );
};


