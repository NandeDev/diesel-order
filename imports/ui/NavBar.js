import React from 'react';
import {Accounts} from "meteor/accounts-base";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import {createContainer} from 'meteor/react-meteor-data'
import {Session} from "meteor/session";

  export const NavBar =  (props) =>{
        const navBarImg = props.isNavOpen ? '/images/x.svg': '/images/bars.svg';
            return (

            <div className='nav-bar'>
                <img className="nav-bar__nav-toggle" src= {navBarImg} onClick={props.imgChange}/>
                {/*<Link className='button button--link' to='/dashboard'>Diesel Order</Link>*/}
                <Link className='button button--link' to='/proof-of-delivery'>Proof of Delivery</Link>
                <Link className='button button--link' to='/maps'>Maps-Routes</Link>
                <Link className='button button--link' to='/Towns'>Towns</Link>
                <Link className='button button--link' to='/addresses'>Addresses</Link>
                <h1 className='nav-bar--title'>{props.title}</h1>
                <button className='button button--logout' onClick={() => props.handleLogout()}>Logout</button>
            </div>
        );
    };

        NavBar.propTypes = {
            title: PropTypes.string.isRequired,
            handleLogout: PropTypes.func,
            imgChange: PropTypes.func
        };

        NavBar.defaultProps = {
            title:'default header'
        };

        //similiar to having the tracker auto run function
        //Keeps track of the sidenav
        export default createContainer(()=>{
            return {handleLogout:()=>{ Accounts.logout(); Session.set('selectedNoteId', undefined)},
                    isNavOpen: Session.get('isNavOpen'),
                    imgChange: ()=>{Session.set('isNavOpen', !Session.get('isNavOpen'))}
            }
        }, NavBar);


