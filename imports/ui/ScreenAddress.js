import React from 'react';
import AddressList from './AddressList';
import AddressAdd from './AddressAdd';
import PrivateHeader from './NavBar';
import AddressEditor from "./AddressEditor";


        export default () =>{
             return (
                             <div>
                                <PrivateHeader title='Addresses'/>
                                 <div className="page-content">
                                     <div className="page-content__sidebar">
                                         <div className="item-list">
                                         <AddressList/>
                                         </div>
                                     </div>
                                     <div className="page-content__main">
                                         <AddressAdd/>
                                     </div>
                                     <div className="">
                                         {/*<AddressEditor/>*/}
                                     </div>
                                 </div>

                            </div>
             );
        };

