const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
});
// Create client with a Promise constructor
const googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyAzw-6fctpR9gty2oQDDv_OTgNRn5mB1Y4',
    Promise: Promise // 'Promise' is the native constructor.
});

// Geocode an address with a promise
googleMapsClient.geocode({address: '1600 Amphitheatre Parkway, Mountain View, CA'}).asPromise()
    .then((response) => {
        console.log(response.json.results);

    })
    .catch((err) => {
        console.log(err);
    });

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});