import React from 'react';
import PropTypes from 'prop-types';
import {createContainer} from 'meteor/react-meteor-data';
import {Session} from 'meteor/session';
import {Pods} from '../api/pods';
import createHistory from 'history/createBrowserHistory';
import { Link } from 'react-router-dom';
import {Towns} from "../api/towns";
import {Addresses} from "../api/addresses";
import {Meteor} from "meteor/meteor";
import Select from 'react-select';

            //First Import your collections from the API so that we can subscribe (Get access) to the collections




            export class PODEditor extends React.Component{
                constructor(props) {
                    super(props);

                    //Uncomment this part if the POD editor starts giving issues

                    // this.state = {
                    //     title: '',
                    //     loadpoint: '',
                    //     offloadpoint: '',
                    //     poddate: '',
                    //     openkms: '',
                    //     closingkms: ''
                    //
                    // };
                }

                //using state allows us to set and manage the state for the body and title
                //State fixes the input cursor bug
                 //using state allows us to set and manage the state for the body and title
                //State fixes the input cursor bug
                HandleTitle(e){
                    const title = e.target.value;
                    this.setState({title});
                    this.props.call('pods.update', this.props.pod._id, { title });
                }
                HandlePODDate(e){
                    const poddate = e.target.value;
                    this.setState({poddate});
                    this.props.call('pods.update', this.props.pod._id, { poddate });
                }
                HandleOpenKMS(e){
                    const openkms = e.target.value;
                    this.setState({openkms});
                    this.props.call('pods.update', this.props.pod._id, { openkms });
                }
                HandleCloseKMS(e){
                    const closingkms = e.target.value;
                    this.setState({closingkms});
                    this.props.call('pods.update', this.props.pod._id, { closingkms });
                }
                HandleLoadTown(e){
                    const loadpoint = e.target.value;
                    this.setState({loadpoint});
                    this.props.call('pods.update', this.props.pod._id, { loadpoint });
                }
                HandleOffLoadTown(e){
                    const offloadpoint = e.target.value;
                    this.setState({offloadpoint});
                    this.props.call('pods.update', this.props.pod._id, { offloadpoint });
                }

                RemoveNote(){
                    this.props.call('pods.remove', this.props.pod._id);
                    this.props.history.push('/proof-of-delivery');
                    Session.set('selectedPodId', '');
                }


                //This component tracks the current and previous POD selected to ensure that the right data is on
                // the input fields for the selected POD
                componentDidUpdate(prevProps, prevState, snapshot) {
                    const currentNoteId = this.props.pod ? this.props.pod._id : undefined;
                    const prevNoteId = prevProps.pod ? prevProps.pod._id : undefined;
                    if(currentNoteId && currentNoteId !== prevNoteId ){

                        //Set the state of the attributes for each POD that is selected To ensure the correct data is displayed
                        this.setState({
                            title:this.props.pod.title,
                            loadpoint:this.props.pod.loadpoint,
                            truckstop:this.props.pod.truckstop,
                            offloadpoint: this.props.pod.offloadpoint,
                            poddate: this.props.pod.poddate,
                            openkms: this.props.pod.openkms,
                            closingkms: this.props.pod.closingkms
                        })

                    }
                }

                //The ComponentDidMount is called immediately after a component is loaded and only works on the client side
                //In here we subscribe to all collections we want access to. Eg towns, users, addresses, diesels


                componentDidMount() {

                    //Step 1 write subscribe method
                    //Step 2 create const and assign the method to fetch all objects in a collection
                    //Step 3 Set the state of the const

                    Meteor.subscribe('towns'); // as referenced in towns.js
                    const towns = Towns.find({}).fetch();
                    this.setState({ towns });

                    Meteor.subscribe('addresses');// as referenced in addresses.js
                    const addresses = Addresses.find({}).fetch();
                    this.setState({ addresses });
                }

                render() {

                    //Step 1 create a const to store the objects of the town collection.
                    const array1 = this.props.town;
                    //Step 2 create a const to map the objects in the array to return only a selected value from each individual object
                    //Eg. {
                    //      Address:'',
                    //      Code:'',
                    //      Name:''
                    // }
                    const map1 = array1.map(x => x['Address']);
                    //By specifying Address means only the Address of each object will be looked at

                    const array2 = this.props.address;
                    const map2 = array2.map(y => y['Address'] );

                    //Step 3 create an array to store the Addresses of each object
                    const towns = [];
                    const addresses=[];

                    //Run a for loop that returns the Array index and value (Address) associated to each object
                    // The entries() method returns an Array Iterator object with key/value pairs.
                    //     For each item in the original array, the new iteration object will contain an array with the index as the key, and the item value as the value:
                    //
                    //         [0, "Grotto Road"]
                    //         [1, "Milner Street"]
                    //         [2, "School Street"]
                    //         [3, "Oscar Road"]

                    for (const [index, value] of map1.entries()) {
                        towns.push(<option key={index}>{value}</option>)
                    }
                    //.push method adds new items to the end of the array.
                    // I use option because im storing it in a dropdown. You can use input, div etc


                    for (const [index, value] of map2.entries()) {
                        addresses.push(<option key={index}>{value}</option>)
                    }
                   // console.log(map1);
                    console.log(towns);

                    if(this.props.pod) {
                        return (<div className="editor">
                            <div className="container">
                                <Select options={towns} />
                            </div>
                            <input className="editor__title" value={this.state.title} onChange={this.HandleTitle.bind(this)}/>

                            <label>Load Point</label>
                            <input list="loadlist"  className="dropdown" onChange={this.HandleLoadTown.bind(this)} value={this.state.loadpoint}/>
                            {/*A datalist is used to store the towns collection and implements a search filter*/}
                            <datalist id="loadlist" className="dropdown">
                                {towns}
                            </datalist>
                            {/*Hidden field to store the state of the town for route viewing*/}
                            <input id="start" className="dropdown" onChange={this.HandleLoadTown.bind(this)} value={this.state.loadpoint} hidden={true}/>
                            <label>Offload Point</label>
                            <input list="offloadlist" className="dropdown" onChange={this. HandleOffLoadTown.bind(this)} value={this.state.offloadpoint}/>
                            {/*A datalist is used to store the towns collection and implements a search filter*/}
                            {/*I can now display the towns array on a dropdown using {towns} */}
                            <datalist id="offloadlist" className="dropdown ">
                                {towns}
                            </datalist>
                            {/*Hidden field to store the state of the town for route viewing*/}
                            <input id="end" className="dropdown" onChange={this. HandleOffLoadTown.bind(this)} value={this.state.offloadpoint} hidden={true}/>

                            <label>Address</label>
                            <input list="addresslist" className="dropdown"/>

                            <datalist id="addresslist" className="dropdown">
                                {addresses}
                            </datalist>
                            <div className="button--clear">
                                <Link className="button button--link"  to='/maps' id='submit' onClick={() =>{var a = document.getElementById('start').value;var b = document.getElementById('end').value; Session.set('value', a);Session.set('value2', b)}}>View Route
                                </Link>
                            </div>
                            <label>POD Date</label>
                            <input type='date' className="editor__input" placeholder='POD date' value={this.state.poddate}
                                   onChange={this.HandlePODDate.bind(this)}/>
                            <label>Opening KMS</label>
                            <input type='number' className="editor__input" placeholder='Opening KMS' value={this.state.openkms}
                                   onChange={this.HandleOpenKMS.bind(this)}/>
                            <label>Closing KMS</label>
                            <input type='number' className="editor__input" placeholder='Closing KMS' value={this.state.closingkms} onChange={this.HandleCloseKMS.bind(this)}/>
                            {/*<label>Map KMS</label>*/}
                            <div className="button--clear">
                                <button className="button" onClick={() => {
                                    if (confirm('Are you sure?') == true) {
                                        this.RemoveNote()
                                    }
                                }}>Delete Order
                                </button>
                            </div>

                        </div>)
                    }else{

                        return <div className="editor">
                            <p className="editor__message">{this.props.selectedPodId ?'No POD found':'Pick or create a new POD'}</p>
                        </div>
                    }

                }

            };

            PODEditor.propTypes = {

                pod: PropTypes.object,
                selectedPodId: PropTypes.string,
                history: PropTypes.object,
                call:PropTypes.func,
                town:PropTypes.object,
                address:PropTypes.object
            };


            export default createContainer(()=>{

                const selectedPodId = Session.get('selectedPodId');
                return{

                    selectedPodId,
                    pod: Pods.findOne(selectedPodId),
                    call:Meteor.call,
                    history: createHistory(),
                    town: Towns.find().fetch(),
                    address: Addresses.find().fetch()
                }

            },PODEditor);



