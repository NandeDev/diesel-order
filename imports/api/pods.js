import {Mongo} from 'meteor/mongo';
import {Meteor} from "meteor/meteor";
import moment from 'moment';
import SimpleSchema from 'simpl-schema';



    export const Pods  = new Mongo.Collection('pods');
    let count = 1;

    if(Meteor.isServer) {

        Meteor.publish('pods',function(){

            return Pods.find({userId:this.userId});
        });

    }
    Meteor.methods({

        'pods.insert'() {

            if (!this.userId) {

                throw new Meteor.Error('User ID is not valid');
            }

            return Pods.insert({

                title: 'POD'+ ' #'+count++,
                loadpoint: '',
                offloadpoint:'',
                poddate: '',
                openkms:'',
                closingkms:'',
                userId: this.userId,
                timestamp: moment().valueOf()
            });
        },

        'pods.remove'(_id) {
            if (!this.userId) {
                throw new Meteor.Error('not-authorized');
            }
            new SimpleSchema({
                _id: {
                    type: String,
                    min: 1
                }
            }).validate({_id});

            Pods.remove({_id, userId: this.userId});
        },

        'pods.update'(_id, updates) {
            if (!this.userId) {
                throw new Meteor.Error('not-authorized');
            }
            new SimpleSchema({
                _id: {
                    type: String,
                    min: 1
                },
                title: {
                    type: String,
                    optional: true
                },
                loadpoint: {
                    type: String,
                    optional: true
                },
                offloadpoint:{
                    type: String,
                    optional: true
                },
                poddate:{
                    type: String,
                    optional: true
                },
                openkms:{
                    type: String,
                    optional: true
                },
                closingkms:{
                    type: String,
                    optional: true
                }

            }).validate({_id, ...updates}); //tripple dots spreads out the updates object. Else simple schema will throw an error

            Pods.update({
                _id,
                userId: this.userId
            }, {
                $set: {
                    timestamp: moment().valueOf(),
                    ...updates
                }
            })

        }

    });
