import {Mongo} from 'meteor/mongo';
import {Meteor} from "meteor/meteor";
import moment from 'moment';
import SimpleSchema from 'simpl-schema';



    export const Diesels  = new Mongo.Collection('diesels');
    let count = 1;

    if(Meteor.isServer) {
        Meteor.publish('diesels',function(){
            return Diesels.find({userId:this.userId});
        });
    }
        Meteor.methods({

            'diesels.insert'() {

                if (!this.userId) {
                    throw new Meteor.Error('User ID is not valid');
                }

                return Diesels.insert({
                    title: 'Diesel Order'+ ' #'+count++,
                    createdby: '',
                    body: '',
                    truckstop:'',
                    speedo: '',
                    price:'',
                    quantity:'',
                    transactiondate: '',
                    userId: this.userId,
                    timestamp: moment().valueOf()
                });
            },

            'diesels.remove'(_id) {
                if (!this.userId) {
                    throw new Meteor.Error('not-authorized');
                }
                new SimpleSchema({
                    _id: {
                        type: String,
                        min: 1
                    }
                }).validate({_id});

                Diesels.remove({_id, userId: this.userId});
            },

            'diesels.update'(_id, updates) {
                if (!this.userId) {
                    throw new Meteor.Error('not-authorized');
                }
                new SimpleSchema({
                    _id: {
                        type: String,
                        min: 1
                    },
                    title: {
                        type: String,
                        optional: true
                    },
                    body: {
                        type: String,
                        optional: true
                    },
                    truckstop:{
                        type: String,
                        optional: true
                    },
                    speedo:{
                        type: String,
                        optional: true
                    },
                    quantity:{
                        type: String,
                        optional: true
                    },
                    price:{
                        type: String,
                        optional: true
                    },
                    transactiondate:{

                        type: String,
                        optional: true
                    },
                    createdby :{
                        type:String,
                        optional:true
                    }
                }).validate({_id, ...updates}); //tripple dots spreads out the updates object. Else simple schema will throw an error

                Diesels.update({
                    _id,
                    userId: this.userId
                }, {
                    $set: {
                        timestamp: moment().valueOf(),
                        ...updates
                    }
                })
            }

        });
