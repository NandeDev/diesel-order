import {Mongo} from 'meteor/mongo';
import {Meteor} from "meteor/meteor";
import moment from 'moment';
import SimpleSchema from 'simpl-schema';
import shortid from "shortid";
import {Addresses} from "./addresses";



    export const Towns  = new Mongo.Collection('towns');


    if(Meteor.isServer) {
        Meteor.publish('towns',function(){
            return Towns.find({userId:this.userId});
        });

    Meteor.methods({

        'towns.insert'(Address,Code,Description,City,Country,Province,Latitude,Longitude){
            //first check to make sure the user is logged in
            if(!this.userId){
                throw new Meteor.Error("Not Authorized");
            }
            //validate the URL
            new SimpleSchema({
                Address:{
                    type: String,
                    min: 1
                    //label: 'Your link',
                    // regEx: SimpleSchema.RegEx.Url
                },
                Code:{
                    type: String,
                    optional:false,
                    min: 1
                },
                Description:{
                    type: String,
                    optional:false,
                    min: 1
                },
                City: {
                    type: String,
                    min: 1
                },
                Country: {
                    type: String,
                    min: 1
                },
                Province: {
                    type: String,
                    min: 1
                },
                Latitude:{
                    type: String,
                    min: 1
                },
                Longitude:{
                    type: String,
                    min: 1

                }
            }).validate({Address,Code,Description,City,Country,Province,Latitude,Longitude});

            Towns.insert({
                Address,
                Code:Code,
                Description:Description,
                Country:Country,
                City:City,
                Province:Province,
                Latitude:Latitude,
                Longitude:Longitude,
                userId: this.userId,
                timestamp: moment().valueOf()

            }); // adds links to the collection(DB)

        },

        'towns.remove'(_id) {
            if (!this.userId) {
                throw new Meteor.Error('not-authorized');
            }
            new SimpleSchema({
                _id: {
                    type: String,
                    min: 1
                }
            }).validate({_id});

            Towns.remove({_id, userId: this.userId});
        },

        'towns.update'(_id, updates) {
            if (!this.userId) {
                throw new Meteor.Error('not-authorized');
            }
            new SimpleSchema({
                _id: {
                    type: String,
                    min: 1
                },
                title: {
                    type: String,
                    optional: true
                },
                towncode: {
                    type: String,
                    optional: true
                },
                townAPIname:{
                    type: String,
                    optional: true
                },
                townAPIprovince:{
                    type: String,
                    optional: true
                },
                address:{
                    type:String,
                    optional: true
                },
                townAPIcountry:{
                    type:String,
                    optional: true
                }
            }).validate({_id, ...updates}); //tripple dots spreads out the updates object. Else simple schema will throw an error

            Towns.update({
                _id,
                userId: this.userId
            }, {
                $set: {
                    timestamp: moment().valueOf(),
                    ...updates
                }
            })

        }

    });
    }
