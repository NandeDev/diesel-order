import {Mongo} from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import SimpleSchema from "simpl-schema";
import shortid from 'shortid';
import moment from "moment";
import {Towns} from "./towns";

export const Addresses = new Mongo.Collection('addresses');

    //if statement to check that you are on the server
   if(Meteor.isServer){
            //Meteor.publish is only available on the server
       Meteor.publish('addresses',function(){
           return Addresses.find({userId:this.userId});
           //Using return Addresses.find({URL: 'Link 3'}); allows you to find by a specific URL
       });
   }
    //naming convention for methods  is resource.action
    //eg links.email
        Meteor.methods({

            'addresses.remove'(URL){
                Addresses.remove({_id: shortid});
            },

           'addresses.insert'(Address,Code,Description){
               //first check to make sure the user is logged in
               if(!this.userId){
                   throw new Meteor.Error("Not Authorized");
               }
               //validate the URL
                   new SimpleSchema({
                    Address:{
                        type: String,
                        optional:false,
                        min:1
                        //label: 'Your link',
                       // regEx: SimpleSchema.RegEx.Url
                    },
                       Code:{
                           type: String,
                           optional:false,
                           min:1

                       },
                       Description:{
                           type: String,
                           optional:false,
                           min:1
                       }
                   }).validate({Address,Code,Description});

               Addresses.insert({
                   //shortid allows us to generate shorter versions of a id
                   Address,
                   Code:Code,
                   Description:Description,
                   userId: this.userId,
                   visible:true,
                   lastVisited: null,
                   visitedCount:0
                   }); // adds links to the collection(DB)

           },
            'links.trackVisit'(_id)
            {
            //Second set up the schema and validate
                new SimpleSchema({
                    _id:{
                        type: String,
                        min: 1
                    }
                }).validate({_id});

                Addresses.update({_id}, {

                    $set: {
                        lastVisited: new Date().getTime()
                    },
                    $inc:{ visitedCount: 1 }
                });

            },
            'addresses.update'(_id, updates) {
                if (!this.userId) {
                    throw new Meteor.Error('not-authorized');
                }
                new SimpleSchema({
                    _id: {
                        type: String,
                        min: 1
                    },
                    address:{
                        type:String,
                        optional: true
                    }
                }).validate({_id, updates}); //tripple dots spreads out the updates object. Else simple schema will throw an error

                Addresses.update({
                    _id,
                    userId: this.userId
                }, {
                    $set: {
                        timestamp: moment().valueOf(),
                        ...updates
                    }
                })

            }

        });





